const bip39 = require('bip39')
const bip32 = require('bitcoinjs-lib').bip32
const crypto = require('bitcoinjs-lib').crypto;
// const ECPair = require('bitcoinjs-lib').ECPair;
// const bitcoin = require('bitcoinjs-lib');
const address = require('bitcoinjs-lib').address;
const network = require('bitcoinjs-lib').networks.testnet;
const wif = require('wif');
const ethUtils = require('ethereumjs-util');

let mnemonic = bip39.generateMnemonic() // this will generate a random phrase
// 'width humor wheat sad obscure outer ancient grab edit labor record express'

let seedBuffer = bip39.mnemonicToSeed('width humor wheat sad obscure outer ancient grab edit labor record express')
// <Buffer 85 19 18 81 cf 0e cc f7 9c 5d 61 ......
// 85191881cf0eccf79c5d6152e8dada72aad5....4cad0e0257f6c83e2e1c380026ed3a78

let masterNode = bip32.fromSeed(seedBuffer);
// xprv9s21ZrQH143K3ZRz2RyyCKLQsY63J1e5ndSnSTiFhNTTp1bcmiTfhRtFU9sYMgg3kBqSWR8ggyPtb744Fw3aVN8Y4DGNtctSzbwbsrHncw2
// xpub661MyMwAqRbcG3WT8TWyZTH9RZvXhUMw9rNPEr7sFhzSgovmKFmvFECjKREAFrLBwfPr4MqtZ9w57xj7apEa7Nxic67hM55wtC8VPYybzKv

// *****************Deriving the first account based on BIP44*********************
let account0 = masterNode.derivePath("m/44'/1'/0'");
// xprv9yCMSddCsB2u4aMZysdnrMbtK1BRx1DqnXeoCii4LDZucuBShdVfTPhAu3MBM77yeCd5vVZVmyfKcdv5JE3suNhERfFReAWnhz9bvmCR9hH
// xpub6CBhr9A6hYbCH4S35uAoDVYcs31vMTwh9kaQ177ftZ6tVhWbFAov1C1ekKwu6KJ4Ydvdmp9cuDArAN9TUFpVjfJwZ9nqKMWcrmbzCcTSqU8

// *****************Deriving the second account based on BIP44*********************
// let account1 = masterNode.derivePath("m/44'/0'/1'");
// xprv9yCMSddCsB2u7HiJcg2G8R5CMprPGvL18N6po9GJ8uPySqh7fTN1THr8K6QWwtEvWB1WNsX5u23tf6QovRXveSnDj5K1SzfjatkJNYPFdV1
// xpub6CBhr9A6hYbCKmnmihZGVZ1vurgsgP3rVb2RbXfuhEvxKe2GCzgG16AcAMoBWw68J7MgXwSmJUPztZsEBs9bMLTf6LQvxgZcXc8ueGEW2kq
let childKey0 = account0.derivePath("0");
let xprivChildKey0 = childKey0.toBase58();
// xprv9zwVDzhBKj8e9uLrNPTJdM2oSSTPgobknjPvmzzV7hbmPgSHJxcWStcZ3f1NbebHBkJVAXXVWY6A57W2KMNuJuDywTMsc9Ro9gSFJkncUgd
let xpubChildKey0 = childKey0.neutered().toBase58();
// xpub6DvqdWE5A6gwNPRKUQzJzUyXzUHt6GKc9xKXaPQ6g38kGUmRrVvkzgw2tv432XwZFpTQvywps33xczXcN1YdAYHJS4SoU9itYSXjiff5VbD

let hdPubNode;
hdPubNode = bip32.fromBase58(childKey0.derivePath("0").neutered().toBase58());
const pubKey = hdPubNode.publicKey.toString('hex');
const publicKeyBuff = new Buffer(pubKey, 'hex');
console.log(`>>>>>>>>>> Public Key >>>>>>>>>> ${pubKey}`);

hdPubNode = bip32.fromBase58(childKey0.derivePath("0").toBase58());
const privKey = hdPubNode.privateKey.toString('hex');
const privateKeyBuff = new Buffer(privKey, 'hex');
const WIFPrivateKey = wif.encode(network.wif, privateKeyBuff, true)
console.log(`>>>>>>> Private Key >>>>>>> ${privKey}`);
console.log(`>>>>>>> WIF Private Key >>>>>>> ${WIFPrivateKey}`);

const addressGen = address.toBase58Check(
  crypto.hash160(publicKeyBuff),
  network.pubKeyHash
);

console.log(`>>>>>>>>> Address >>>>>>>> ${addressGen}` );

let ethAddress = ethUtils.addHexPrefix(ethUtils.privateToAddress(privateKeyBuff).toString("hex"));
console.log(`>>>>>>>>> ETH Address >>>>>>>>> ${ethAddress}`);

///////////////////////////////////////////////////////////////////////////////////////////////
// ETH Private Key => 919e177273abcd5ef0f3d5b4697b199af8109dbecba42c94b9c134b7ebc9e7a4
// ETH Public Key => 03c804f83fbecf2750e4b8d6cd7557a5a16863f24ebbf9b67e250f924c49a3eed4
// ETH Address => 0x8985fedb7208840b55f4f94968e312bafab7806b
///////////////////////////////////////////////////////////////////////////////////////////////

/* ETH Transaction */
/* 
const Web3 = require('web3');
const Tx = require('ethereumjs-tx');
web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/90bfb15e855e41a8997b24d5f7a170e1'));
const gasPrice = "2";
const gasLimit = 3000000;
const addr = "0x7Addf64c0DB31eBC40f91c11e8b4c883215A32ce";
const toAddress = "0x9aa9D191EFf22A8EbCC2dD04542F3F4A283101b3";
const amountToSend = web3.utils.toHex(web3.utils.toWei("1", "ether"));
const nonce = web3.eth.getTransactionCount(addr);
let rawTransaction = {
"from": addr,
"nonce": web3.utils.toHex(nonce),
"gasPrice": web3.utils.toHex(gasPrice * 1e9),
"gasLimit": web3.utils.toHex(gasLimit),
"to": toAddress,
"value": amountToSend,
"chainId": 3 //remember to change this
};
const privateKey = "4da0faf4853ba46db1d5fa438bc774d6b51422a56325a4fe2d70bf6243060ca9";
const privKeyBuff = new Buffer(privateKey, 'hex');
console.log(`privKey : ${privKeyBuff}`);
let tx = new Tx(rawTransaction);
tx.sign(privKeyBuff);
let serializedTx = tx.serialize();

console.log(`serializedTx : ${serializedTx}`);

web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
  if (!err)
    console.log(`Txn Sent and hash is ${hash}`);
  else
    console.error(err);
});
 */

///////////////////////////////////////////////////////////////////////////////////////////////
// BTC Testnet Public Key => 021ca409fc574aff1b61c3381f8b09afd92a17a96574d1077a83e9dcd13119dc14
// BTC Testnet Private Key => 14a74a0b859034d9e6775eb4b01b114ae50762185d9dcab2f935487cf39b08ae
// BTC Testnet WIF Private Key => cNGrDH9MXyuKUMSDEdwQGfShq6Teg5dajAJ4hLePGxVGDNRJNMw1
// BTC Testnet Address => mk2covL3mw2UauEmZPKLuSBcrezeJGCJWS
///////////////////////////////////////////////////////////////////////////////////////////////

/* BTC Testnet Transaction */
/* 
const bitcoin = require('bitcoinjs-lib');
let ECKeyPair = bitcoin.ECPair.fromWIF("cNGrDH9MXyuKUMSDEdwQGfShq6Teg5dajAJ4hLePGxVGDNRJNMw1", bitcoin.networks.testnet);

let tx = new bitcoin.TransactionBuilder(bitcoin.networks.testnet);
let totalAmount = 130000000 // 1.3 BTC
let amountToKeep = 100000000 // 1 BTC
let transactionFee = 1000 // .00001 BTC
let amountToSend = totalAmount - amountToKeep - transactionFee
tx.addInput("3bcf15676863f77c1b48b35e0eb5d7a5ddecb3cb60f20e71c84061b2c8663a23", 0);
tx.addOutput("mnizBz33sPb4fsLLjfB2p6phmCQGHqr5tZ", amountToSend);
tx.addOutput("mk2covL3mw2UauEmZPKLuSBcrezeJGCJWS", amountToKeep);
tx.sign(0, ECKeyPair);
let tx_hex = tx.build().toHex()
console.log(tx_hex);
 */
/*
To broadcast transaction
  url: POST https://test-insight.bitpay.com/api/tx/send
  body: {
    // "rawtx": transaction hash in hex
	  "rawtx": "0200000001233a66c8b26140c8710ef260cbb3ecdda5d7b50e5eb3481b7cf763686715cf3b000000006b483045022100c8022a3fade5c7331c94bb1e3790124fe105751f931cc5973b27b06063c2ad3d02206c8fcb3bf0c2b5af6e64b1e9e970d64eea96ce22141b893b4ff47623eff4332a0121021ca409fc574aff1b61c3381f8b09afd92a17a96574d1077a83e9dcd13119dc14ffffffff0298bfc901000000001976a9144f0f74456eab5cb23051e498b5ff52edd536fd0988ac00e1f505000000001976a914317cb778131ddd97c7519df239a45ed5c263f96488ac00000000"
  }
*/
